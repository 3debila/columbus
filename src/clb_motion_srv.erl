%%%-------------------------------------------------------------------
%%% @author Valery Meleshkin
%%% @copyright 2014
%%%-------------------------------------------------------------------

-module(clb_motion_srv).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
-include("../include/columbus_messages.hrl").

%% API Function Exports
-export([start_link/0]).

%% gen_server Function Exports
-export([init/1, handle_call/3, handle_cast/2, 
         handle_info/2, terminate/2, code_change/3]).

%% API Function Definitions

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% gen_server Function Definitions

init(Args) ->
    self() ! init,
    {ok, Args}.

handle_call(_Request, _From, State) ->
    {noreply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(init, State) ->
    {ok, Wheels} = application:get_env(wheels),
    [{ok, _P} = clb_wheel_enc_sup:start_wheel_enc(WID, ?SERVER) 
     || {WID, _Conf} <- Wheels],
    {noreply, State};

handle_info(?CLB_WHEEL_MOVE(WheelID, Move), State) ->
    error_logger:info_report([{motion_server, self()},
                              {what, weel},
                              {id, WheelID},
                              {event, {move, Move}}
                             ]),
    {noreply, State};

handle_info(?CLB_WHEEL_ERROR(WheelID), State) ->
    error_logger:info_report([{motion_server, self()},
                              {what, weel},
                              {id, WheelID},
                              {event, error}
                             ]),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% Internal Function Definitions

