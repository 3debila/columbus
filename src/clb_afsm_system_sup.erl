%%%-------------------------------------------------------------------
%%% @author Valery Meleshkin <valery.meleshkin@gmail.com>
%%%-------------------------------------------------------------------

-module(clb_afsm_system_sup).

-behaviour(supervisor).

%% API
-export([start_link/2]).

%% Supervisor callbacks
-export([init/1]).

%% ------------------------------------------------------------------
%% Helper macro for declaring children of supervisor
%% ------------------------------------------------------------------

-define(CHILDW(I, M, Args), 
        {I, {M, start_link, Args}, transient, 5000, worker, [I]}).

%% ------------------------------------------------------------------
%% API functions
%% ------------------------------------------------------------------

start_link(SysName, {system, Insts, Bindings}) ->
    Spec = lists:map(fun({AFSMName, {proc, Mod, Opts}}) ->
        ?CHILDW(AFSMName,
                clb_afsm_impl,
                [AFSMName,
                 releveant_bindings(AFSMName, Bindings),
                 Mod, Opts])
    end, Insts),
    supervisor:start_link({local, SysName}, ?MODULE, Spec).

%% ------------------------------------------------------------------
%% Supervisor callbacks
%% ------------------------------------------------------------------

init(Spec) ->
    {ok, { {one_for_one, 5, 10}, Spec} }.

is_relevant_fsm(RFSM, {normal, {RFSM, _}, _Target}) -> true;
is_relevant_fsm(RFSM, {supressing, _Time, _Supressor, {RFSM, _Port}}) -> true;
is_relevant_fsm(RFSM, {supressing, _Time, {RFSM, _Port}, _Target}) -> true;
is_relevant_fsm(RFSM, {inhibiting, _Time, _Inhibitor, {RFSM, _Port}}) -> true;
is_relevant_fsm(RFSM, {inhibiting, _Time, {RFSM, _Port}, _Inhibited}) -> true;
is_relevant_fsm(_FSM, {normal, {_FSM1, _Port1}, {_FSM2, _Port2}}) -> false;
is_relevant_fsm(_FSM, {supressing, _Time, {_FSM1, _Port1}, {_FSM2, _Port2}}) -> false;
is_relevant_fsm(_FSM, {inhibiting, _Time, {_FSM1, _Port1}, {_FSM2, _Port2}}) -> false;
is_relevant_fsm(_FSM, Binding) -> error({badarg, Binding}).

releveant_bindings(AFSMName, Bindings) ->
    [B || B <- Bindings, is_relevant_fsm(AFSMName, B)].

