%%%-------------------------------------------------------------------
%%% @author Valery Meleshkin <valery.meleshkin@gmail.com>
%%%-------------------------------------------------------------------

-module(clb_afsm_registry).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
-define(ETS, ?MODULE).

%% API Function Exports
-export([start_link/0]).
-export([register_name/2,
         unregister_name/1,
         whereis_name/1,
         send/2
        ]).

%% gen_server Function Exports
-export([init/1, handle_call/3, handle_cast/2, 
         handle_info/2, terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

register_name(Name, Pid) when is_pid(Pid) ->
    gen_server:call(?SERVER, {register_name, Name, Pid}).

unregister_name(Name) ->
    gen_server:cast(?SERVER, {unregister_name, Name}).

whereis_name(Name) ->
    try ets:lookup_element(?ETS, Name, 2)
    catch _:_ -> undefined end.

send(Name, Msg) ->
    case whereis_name(Name) of
        undefined ->
            error({badarg, {Name, Msg}});
        Pid when is_pid(Pid) ->
            Pid ! Msg,
            Pid
    end.

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

-record(state, { pid2name, name2pid }).

init([]) ->
    N2P = ets:new(?ETS, [protected, named_table,
                         {read_concurrency, true}]),
    P2N = ets:new(clb_afsm_reg_pid2name, [private, bag]),
    {ok, #state{ pid2name = P2N, name2pid = N2P }}.

handle_call({register_name, Name, Pid}, _From, 
            #state{ pid2name = P2N } = State) ->
    Res = case ets:insert_new(?ETS, {Name, Pid}) of
              false -> no;
              true ->
                  MRef = monitor(process, Pid),
                  ets:insert(P2N, {Pid, Name, MRef}),
                  yes
          end,
    {reply, Res, State}.

handle_cast({unregister_name, Name},
            #state{ pid2name = P2N } = State) ->
    case whereis_name(Name) of
        undefined -> ok;
        Pid when is_pid(Pid) ->
            ets:delete(?ETS, Name),
            [Ref] =  [Ref || {P, Nm, Ref} <- ets:lookup(P2N, Pid),
                             Nm == Name, P == Pid],
            demonitor(Ref),
            ets:delete_object(P2N, {Pid, Name, Ref})
    end,
    {noreply, State};
handle_cast(_Msg, State) ->
    error_logger:warning_msg("Unexpected ~p~n", [_Msg]),
    {noreply, State}.

handle_info({'DOWN', _MRef, process, Pid, _Info},
            #state{ pid2name = P2N } = State) ->
    NamesRefs = ets:lookup(P2N, Pid),
    [ets:delete(?ETS, Nm) || {_Pid, Nm, _Ref} <- NamesRefs],
    ets:delete(P2N, Pid),
    {noreply, State};
handle_info(_Info, State) ->
    error_logger:warning_msg("Unexpected ~p~n", [_Info]),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

