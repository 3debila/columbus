-module(clb_device_info).
-define(APP_NAME, columbus).

-export([get_device_props/1, get_param/2]).

get_device_props(Device) ->
    {ok, Devices} = get_env(devices),
    {ok, DefProps} = get_env(default_props),
    DeviceProps = proplists:get_value(Device, Devices, []),
    lists:ukeymerge(1, DeviceProps, DefProps).

get_param(Device, Param) ->
    proplists:get_value(Param, get_device_props(Device)).

-ifndef(TEST).

get_env(Key) -> application:get_env(?APP_NAME, Key).

-else.

get_env(_Key) -> {ok, foo}.

-endif(TEST).
