-module(clb_afsm_hwin).
-behaviour(clb_afsm).
-export([init/2, handle_signal/4, handle_info/3]).


init(_Name, Props) -> 
    Method = proplists:get_value(method, Props),
    true = lists:member(Method, [get_int, get_float, collect_value]),
    {ok, the_only_state, [in], [out], Props}.

handle_signal(_Val, in, the_only_state, ModuleState) ->
    Device = proplists:get_value(device, ModuleState),
    Channel = proplists:get_value(channel, ModuleState),
    Method = proplists:get_value(method, ModuleState),
    Controller = clb_device_info:get_param(Device, controller),
    {ok, Ret} = Controller:Method(Device, Channel),
    {ok, [{out, Ret}], ModuleState}.

handle_info(_, the_only_state, ModuleState) ->
    {ok, ModuleState}.
