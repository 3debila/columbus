%%%-------------------------------------------------------------------
%%% @author Valery Meleshkin <valery.meleshkin@gmail.com>
%%%-------------------------------------------------------------------

-module(clb_controller_duino).
-behaviour(gen_server).
-behaviour(clb_controller).
-include("../include/clb_controller.hrl").
-include("../include/clb_controller_proto.hrl").

-define(INITIALIZATION_DELAY, 1000).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

%% API Function Exports
-export([start_link/1]).
-export([get_int/2, get_float/2, set/3, collect_value/2,
         trigger_threshold/6, trigger_threshold/4, cancel_trigger/2]).
-compile({no_auto_import,[demonitor/1]}).
-export([monitor/1, demonitor/1]).
%% Low-level API
-export([send/3, unsubscribe/2]).

%% gen_server Function Exports
-export([init/1, handle_call/3, handle_cast/2, 
         handle_info/2, terminate/2, code_change/3]).

%%% API Function Definitions

get_int(Device, Channel) ->
    send_decode(Device, 1, <<Channel:8/integer>>, fun(<<IntRes:16/signed-little-integer>>) -> IntRes end).

get_float(Device, Channel) ->
    send_decode(Device, 1, <<Channel:8/integer>>, fun(<<IntRes:16/signed-little-integer>>) ->
                                                          IntRes/1024
                                                  end).

set(Device, Channel, Value) ->
    IntValue = value_to_int(Value),
    send_decode(Device, 2, <<Channel:8/integer, IntValue:16/little-integer>>, discard).

collect_value(Device, Channel) ->
    send_decode(Device, 3, <<Channel:8/integer>>, fun(<<IntRes:16/signed-little-integer>>) -> {int, IntRes} end).

trigger_threshold(Device, OutC, Sign, OutThres, InC, InV) ->
    SignVal = case Sign of
                  '>=' ->
                      1;
                  '=<' ->
                      -1
              end,
    IntOutThres = value_to_int(OutThres),
    IntInV = value_to_int(InV),
    Msg = <<OutC:8/signed-integer, SignVal:8/signed-integer, IntOutThres:16/signed-little-integer,
            InC:8/signed-integer, IntInV:16/signed-little-integer>>,
    send_decode(Device, 4, Msg, fun(<<IntRes:16/little-integer>>) -> IntRes end).

trigger_threshold(Device, OutC, Sign, OutThres) ->
    trigger_threshold(Device, OutC, Sign, OutThres, -1, {int, 0}).

cancel_trigger(Device, TrigId) ->
    send_decode(Device, 5, <<TrigId:16/little-integer>>, discard).

monitor(Device) ->
    monitor(process, device_to_name(Device)).

demonitor(MRef) ->
    demonitor(MRef).


%%% Low-level API

    start_link(Device) ->
    gen_server:start_link({local, device_to_name(Device)}, ?MODULE, [{device, Device}], []).

send_decode(Device, OpCode, Payload, Decoder) ->
    Res = case send(Device, OpCode, Payload) of
              {error, _} ->
                  {error, unsupported};
              {ok, R} ->
                  {ok, R};
              {ok, R, _Trig} ->
                  {ok, R}
          end,
    case Res of
        {ok, <<>>} ->
            {error, unsupported};
        {ok, _} when Decoder == discard ->
            ok;
        {ok, Result} when is_function(Decoder) ->
            {ok, Decoder(Result)};
        {error, _} ->
            {error, unsupported}
    end.

send(Device, OpCode, Payload)
  when is_atom(Device),
       is_integer(OpCode),
       OpCode > ?HSHK_FRAME, OpCode < ?DEBG_FRAME,
       is_binary(Payload), byte_size(Payload) =< 255 ->
    gen_server:call(device_to_name(Device), {send, OpCode, Payload}).

unsubscribe(Device, TriggerId) ->
    gen_server:cast(device_to_name(Device),
                    {unsubscribe, self(), TriggerId}).

%%% gen_server Function Definitions


-record(subscription, { pid, ref, trid }).
-record(state, { tty = undefined,
                 uid2from = orddict:new(), % UniqId -> From (pid + ref)
                 uid2trigs = orddict:new(), % UniqId -> [TriggerId] created during UniqId req
                 deffered = queue:new(),
                 subscriptions = ets:new(?MODULE, test_ets([bag, {keypos, #subscription.trid}])), % TriggerId -> #subscription{}
                 sent = [],
                 ready = false,
                 buffer = <<>>
               }).

init([{device, Device}]) ->
    erlang:send_after(?INITIALIZATION_DELAY, self(), {init, Device}),
    {ok, #state{}}.

handle_call({send, OpCode, Payload}, From,
            #state{ uid2from = U2F,
                    deffered = Def
                  } = State) ->
    Range = pow(2, ?UID_SZ),
    UniqId = erlang:phash2(From, Range),
    PayloadSize = byte_size(Payload),
    Cmd = ?CMD_FRAME(OpCode, UniqId, PayloadSize, Payload),
    {noreply, maybe_send(State#state{ uid2from = orddict:store(UniqId, From, U2F),
                                      deffered = queue:in(Cmd, Def)
                                    })}.

handle_cast({unsubscribe, Pid, TriggerId}, State) ->
    {noreply, unsubscribe(Pid, TriggerId, State)};
handle_cast(stop, State) ->
    {stop, normal, State}.

handle_info({'DOWN', _MRef, process, Pid, _Info},
            #state{ subscriptions = Subs } = State) ->
    ets:select_delete(Subs, [{#subscription{ pid = Pid, _ = '_' },[],[true]}]),
    {noreply, State};
handle_info({init, Device}, State) ->
    Props = clb_device_info:get_device_props(Device),
    {ok, Port} = start_serial(Props),
    {noreply, State#state{ tty = Port }};
handle_info({tty, _Port, _Data},
            #state{ tty = undefined } = State) ->
    {stop, unexpected_data, State};
handle_info({tty, Port, Data},
            #state{ tty = Port,
                    buffer = Buf } = State) ->
    %error_logger:info_report([{incoming, Data}, {buf, Buf}]),
    Buf1 = <<Buf/binary, Data/binary>>,
    {noreply, maybe_send(process_incoming(State#state{ buffer = Buf1 }))}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%% Internal Function Definitions

value_to_int({int, IntVal}) ->
  IntVal;
value_to_int({float, FloatVal}) ->
  trunc(FloatVal*1024).

process_incoming(#state{ tty = Port,
                         sent = Sent,
                         uid2from = U2F,
                         uid2trigs = U2T,
                         ready = IsReady,
                         buffer = Buf } = State) ->
    case Buf of
        ?RESP_FRAME(UniqId, PayloadSize, Rest) when byte_size(Rest) >= PayloadSize ->
            <<Payload:PayloadSize/binary, Rest1/binary>> = Rest,
            State1 = case orddict:find(UniqId, U2F) of
                         error -> State;
                         {ok, {Pid, _Tag} = From} when is_pid(Pid) -> 
                             case orddict:find(UniqId, U2T) of
                                 error -> 
                                     gen_server:reply(From, {ok, Payload}),
                                     State;
                                 {ok, Trigs} when is_list(Trigs) ->
                                     S = lists:foldl(
                                           fun(TID, St) ->
                                                   subscribe(Pid, TID, St)
                                           end, State, Trigs),
                                     gen_server:reply(From, {ok, Payload, Trigs}),
                                     S
                             end
                     end,
            process_incoming(State1#state{ uid2from = orddict:erase(UniqId, U2F),
                                           uid2trigs = orddict:erase(UniqId, U2T),
                                           sent = lists:delete(UniqId, Sent),
                                           buffer = Rest1
                                         });
        ?DEBUG_FRAME(PayloadSize, Rest) when byte_size(Rest) >= PayloadSize ->
            <<Payload:PayloadSize/binary, Rest1/binary>> = Rest,
            Payload1 = case (PayloadSize > 0) and (binary:last(Payload) == 0) of
                           true -> binary:part(Payload, 0, PayloadSize - 1);
                           false -> Payload
                       end,
            error_logger:info_report([{frame, debug}, {data, Payload1}, {from, Port}]),
            process_incoming(State#state{ buffer = Rest1 });
        ?NEW_TRIGGER_FRAME(UniqId, TriggerId, Rest) ->
            process_incoming(State#state{ buffer = Rest,
                                          uid2trigs = orddict:append(UniqId, TriggerId, U2T)
                                        });
        ?TRIGGER_FRAME(TriggerId, Rest) ->
            [P ! ?TRIGGER(TriggerId)
             || #subscription{ pid = P } <- ets:lookup(State#state.subscriptions, TriggerId)],
            %error_logger:info_report([{frame, trigger}, {tid, TriggerId}, {from, Port}]),
            process_incoming(State#state{ buffer = Rest });
        ?HANDSHAKE(Rest) when not(IsReady) ->
            process_incoming(State#state{ buffer = Rest, ready = true });
        ?HANDSHAKE(_Rest) ->
            exit(second_handshake);
        Rest when is_binary(Rest) ->
            State#state{ buffer = Rest }
    end.

subscribe(Pid, TriggerId,
          #state{ subscriptions = Subs } = State) ->
    Subs4Id = ets:lookup(Subs, TriggerId),
    case lists:keyfind(Pid, #subscription.pid, Subs4Id) of
        false ->
            MRef = monitor(process, Pid),
            ets:insert(Subs, #subscription{ pid = Pid, ref = MRef, trid = TriggerId });
        #subscription{} ->
            ok
    end, State.

unsubscribe(Pid, TriggerId,
            #state{ subscriptions = Subs } = State) ->
    Subs4Id = ets:lookup(Subs, TriggerId),
    case lists:keyfind(Pid, #subscription.pid, Subs4Id) of
        false ->
            ok;
        #subscription{ pid = Pid, ref = MRef } = SObj ->
            ets:delete_object(Subs, SObj),
            demonitor(MRef)
    end, State.

pow(_, 0) -> 1;
pow(X, 1) -> X;
pow(X, Y) when is_integer(Y), Y > 0 -> X*pow(X, Y - 1).

device_to_name(Device) when is_atom(Device) ->
    case get({?MODULE, dev2name, Device}) of
        undefined ->
            Name = list_to_atom(atom_to_list(?MODULE) ++ "__" ++ atom_to_list(Device)),
            put({?MODULE, dev2name, Device}, Name),
            Name;
        Name when is_atom(Name) ->
            Name
    end.

maybe_send(#state{ sent = [],
                   ready = true,
                   tty = Port,
                   deffered = DQ
                 } = State) ->
    case queue:out(DQ) of
        {empty, _}  -> State;
        {{value, Cmd}, DQ1} ->
            %% TODO send cancel all triggers first time (handshake)
            ?CMD_FRAME(_OpCode, UniqId, _PSz, _P) = Cmd,
            tty:send(Port, Cmd),
            State#state{ sent = [UniqId],
                         deffered = DQ1 }
    end;
maybe_send(#state{} = State) -> State.

%%% Test 

-ifndef(TEST).

start_serial(Opts) -> tty:start_link(Opts).
test_ets(Opts) -> Opts.

-else.

test_ets(Opts) -> [named_table, protected | Opts].

-define(TRIGGER_DELAY, 100).

start_serial(_Opts) ->
    Owner = self(),
    {ok, spawn_link(
           fun() -> 
                   Owner ! {tty, self(), ?HANDSHAKE(<<>>)},
                   serial_mock(Owner)
           end)}.

serial_mock(Owner) ->
    TrigId = 10,
    receive
        {'$gen_cast', ?CMD_FRAME(OpCode, UniqId, _, _)} ->
            case OpCode of
                1 ->
                    Owner ! {tty, self(), ?DEBUG_FRAME(2, <<1>>)},
                    Owner ! {tty, self(), <<2>>},
                    Owner ! {tty, self(), ?RESP_FRAME(UniqId, 1, <<1>>)};
                2 ->
                    Dbg = ?DEBUG_FRAME(2, <<1, 2>>),
                    Res = ?RESP_FRAME(UniqId, 1, <<1>>),
                    Owner ! {tty, self(), <<Dbg/binary, Res/binary>>};
                3 ->
                    Owner ! {tty, self(), ?NEW_TRIGGER_FRAME(UniqId, TrigId, <<>>)},
                    Owner ! {tty, self(), ?RESP_FRAME(UniqId, 1, <<1>>)},
                    timer:sleep(?TRIGGER_DELAY),
                    Owner ! {tty, self(), ?TRIGGER_FRAME(TrigId, <<>>)};
                4 ->
                    Owner ! {tty, self(), ?RESP_FRAME(UniqId, 1, <<1>>)},
                    timer:sleep(?TRIGGER_DELAY),
                    Owner ! {tty, self(), ?TRIGGER_FRAME(TrigId, <<>>)}
            end,
            serial_mock(Owner);
        stop -> ok
    end.

stop_controller(P) ->
    MRef = monitor(process, P),
    gen_server:cast(P, stop),
    receive {'DOWN', MRef, process, P, _} -> ok
    after 1000 -> error(nostop) end.

cmd_test() ->
    {ok, P} = start_link(foo),
    %% frames splitted into multiple packets
    {ok, <<1>>} = send(foo, 1, <<1>>),
    %% many frames in one packet
    {ok, <<1>>} = send(foo, 2, <<>>),
    %% trigger (un)subscription
    {ok, <<1>>, [Id]} = send(foo, 3, <<>>),
    receive 
        ?TRIGGER(Id) -> ok;
        Wtf -> exit({wtf, Wtf})
    after ?TRIGGER_DELAY*2 -> exit(no_trigger) end,
    unsubscribe(foo, Id),
    {ok, <<1>>} = send(foo, 4, <<>>),
    receive Wtf2 -> exit({wtf, Wtf2})
    after ?TRIGGER_DELAY*2 -> ok end,
    %% auto unsubscription
    {ok, <<1>>, [Id]} = send(foo, 3, <<>>),
    {SP, _} = spawn_monitor(fun() -> send(foo, 3, <<>>) end),
    receive {'DOWN', _, _, SP, _} -> ok
    after 1000 -> error(no_down) end,
    false = lists:keyfind(SP, #subscription.pid, ets:tab2list(?MODULE)),
    unsubscribe(foo, Id),
    %%
    stop_controller(P).

-endif.

