-module(clb_afsm_hwout).
-behaviour(clb_afsm).
-export([init/2, handle_signal/4, handle_info/3]).

init(_Name, Props) -> 
    {ok, the_only_state, [in], [], Props}.

handle_signal(Val, in, the_only_state, ModuleState) ->
    Device = proplists:get_value(device, ModuleState),
    Channel = proplists:get_value(channel, ModuleState),
    Controller = clb_device_info:get_param(Device, controller),
    Controller:set(Device, Channel, Val),
    {ok, ModuleState}.

handle_info(_, the_only_state, ModuleState) ->
    {ok, ModuleState}.
