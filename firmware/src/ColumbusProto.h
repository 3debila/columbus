#ifndef _COLUMBUS_PROTO_H_
#define _COLUMBUS_PROTO_H_

#define PACKED __attribute__((__packed__))
#define ASSERT(cond) typedef int foo[(cond) ? 1 : -1]
#define TYPICAL_VAL_SZ 4

namespace columbus {

	enum opcode
	{
		handshake_frame = 0,

		get = 1,
		set = 2,
		collect_value = 3,
		trigger_threshold = 4,
		cancel_trigger = 5,
		cancel_all_triggers = 6,

		new_trigger_frame = 124,
		resp_frame = 125,
		debug_frame = 126,
		trigger_frame = 127
	};

	typedef uint8_t vsn_opcode_t;
	uint8_t get_version(vsn_opcode_t vsn_opcode) {
		return vsn_opcode & (1 << 7);
	}
	opcode get_opcode(vsn_opcode_t vsn_opcode) {
		vsn_opcode &= ~(1 << 7);
		return static_cast<opcode>(vsn_opcode);
	}
	vsn_opcode_t build_vsn_opcode(opcode code) {
		// vsn bit is just a 0
		vsn_opcode_t vcode = code;
		vcode &= ~(1 << 7);
		return vcode;
	}

	template <class Out>
	class send
	{
		inline void send_uid(const uint16_t &uid) {
			dst.write(reinterpret_cast<const uint8_t *>(&uid), 2);
		}

		template <typename ValT>
		inline void send_payload(const ValT &val) {
			ASSERT(sizeof(ValT) <= 255);
			uint8_t sz = sizeof(ValT);
			dst.write(sz);
			dst.write(reinterpret_cast<const uint8_t *>(&val), sz);
		}

		Out &dst;
	public:
		send(Out &out) : dst(out) {};

		template <typename T>
		void result(const uint16_t &uniq_id, const T &val) {
			dst.write(build_vsn_opcode(resp_frame));
			send_uid(uniq_id);
			send_payload(val);
		}

		void result_null(const uint16_t &uniq_id) {
			dst.write(build_vsn_opcode(resp_frame));
			send_uid(uniq_id);
			uint8_t sz = 0;
			dst.write(sz);
		}

		template <typename T>
		void debug(const T &val) {
			dst.write(build_vsn_opcode(debug_frame));
			send_payload(val);
		}

		void new_trigger(const uint16_t &uniq_id, const uint16_t &trigger_id) {
			dst.write(build_vsn_opcode(new_trigger_frame));
			send_uid(uniq_id);
			send_uid(trigger_id);
		}

		void trigger(const uint16_t &trigger_id) {
			dst.write(build_vsn_opcode(trigger_frame));
			send_uid(trigger_id);
		}

		void handshake() {
			dst.write(build_vsn_opcode(handshake_frame));
		}

		template <typename T>
		void report_error(const uint16_t &uniq_id, const T &err) {
			debug(err);
			result_null(uniq_id);
		}

	};

	template <class Handler>
	class receive
	{
		enum state { st_header, st_payload };
		struct PACKED cmd_header {
			uint8_t vsn_opcode;
			uint16_t uniq_id;
			uint8_t payload_sz;
			cmd_header() : vsn_opcode(0), uniq_id(0), payload_sz(0) {}
		};
		Handler _handler;
		cmd_header _hdr;
		state _state;
		char _buf[TYPICAL_VAL_SZ];
	public:
		receive() : _state(st_header) {};
		
		Handler &handler() { return _handler; };
		const Handler &handler() const { return _handler; };

		template <class StreamT>
		void process_bytes(StreamT &src) {
			const size_t header_sz = sizeof(cmd_header);
			switch (_state) {
				case st_header:
					if (src.available() < header_sz)
						return;
					src.readBytes(reinterpret_cast<char *>(&_hdr), header_sz);
					if (get_version(_hdr.vsn_opcode) == 0) {
						_state = st_payload;
					}
				case st_payload:
					if (_hdr.payload_sz != 0 && src.available() < _hdr.payload_sz)
						return;
					char *payload = NULL;
					if (_hdr.payload_sz > 0) {
						if (_hdr.payload_sz > TYPICAL_VAL_SZ) {
							payload = (char *)malloc(_hdr.payload_sz);
						}
						else {
							payload = _buf;
						}
						src.readBytes(payload, _hdr.payload_sz);
					}
					_handler.on_message(get_opcode(_hdr.vsn_opcode), _hdr.uniq_id, _hdr.payload_sz, payload);
					if (payload != NULL && _hdr.payload_sz > TYPICAL_VAL_SZ)
						free(payload);
					_state = st_header;
			}
		}
	};

}

#endif
