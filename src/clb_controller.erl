-module(clb_controller).

-type device() :: atom().
-type channel_id() :: non_neg_integer().
-type value() :: {float, float()} | {int, integer()}.
-type trigger_id() :: non_neg_integer().

%% Basic ops
-callback get_int(device(), channel_id()) -> {ok, integer()} | {error, unsupported}.
-callback get_float(device(), channel_id()) -> {ok, float()} | {error, unsupported}.
-callback set(device(), channel_id(), value()) -> ok | {error, unsupported}.
-callback collect_value(device(), channel_id()) -> {ok, value()} | {error, unsupported}. %% atomic get + set(0.0)

%% Sets `InV' on `InC' channel when `erlang:Sign(OutC, OutThres) == true' and sends
%% `?TRIGGER_FRAME' with `TriggerId' that was returned by the call.
-callback trigger_threshold(device(), 
                            OutC :: channel_id(), Sign :: '>=' | '=<', OutThres :: value(),
                            InC :: channel_id(), InV :: value()) -> 
    {ok, TriggerId :: trigger_id()}.
-callback trigger_threshold(device(), 
                            OutC :: channel_id(), Sign :: '>=' | '=<', OutThres :: value()) ->
    {ok, TriggerId :: trigger_id()}.
-callback cancel_trigger(device(), trigger_id()) -> ok.

-callback monitor(device()) -> reference().
-callback demonitor(reference()) -> ok.
