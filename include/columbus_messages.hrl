-ifndef(CLB_MESSAGES).
-define(CLB_MESSAGES, true).

-define(CLB_WHEEL_MOVE(WheelID, X), 
        {columbus_wheel_move, WheelID, X}).
-define(CLB_WHEEL_ERROR(WheelID),
        {columbus_wheel_error, WheelID}).

-endif.
