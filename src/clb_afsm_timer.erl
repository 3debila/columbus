-module(clb_afsm_timer).
-behaviour(clb_afsm).
-export([init/2, handle_signal/4, handle_info/3]).

-record(state, {timer_id, timer_interval}).

init(_Name, Props) -> 
    Interval = proplists:get_value(interval, Props),
    {ok, the_only_state, [], [out], #state{timer_id = erlang:send_after(Interval, self(), tick),
                                             timer_interval = Interval}}.

handle_signal(_Val, in, the_only_state, ModuleState) ->
    {ok, ModuleState}.

handle_info(tick, the_only_state, #state{timer_id = OldTimer, timer_interval = Interval}) ->
    erlang:cancel_timer(OldTimer),
    NewTimer = erlang:send_after(Interval, self(), tick),
    {ok, [{out, 1}], #state{timer_id = NewTimer, timer_interval = Interval}};
handle_info(_, the_only_state, ModuleState) ->
    {ok, ModuleState}.

