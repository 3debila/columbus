-module(clb_afsm_impl).

-behaviour(gen_server).

%% API
-export([start_link/4]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {
    name         :: clb_afsm:afsm_name(),
    module       :: module(),
    mod_state    :: term(),
    afsm_state   :: clb_afsm:afsm_state(),
    in_ports     :: [clb_afsm:afsm_port()],
    out_ports    :: [clb_afsm:afsm_port()],
    dispatchtab  :: dict(), % OutPort -> [{normal, {DstName, DstPort}} | {inhibits, {DstName, DstPort}} | {suppresses, {DstName, DstPort}}]
    inhibitortab :: dict(), % OutPort -> [{InhibitorEndPoint, Time}]
    supressortab :: dict(), % InPort -> [{SupressorEndPoint, Time}]
    inhibited    :: dict(), % OutPort -> EndTime
    supressed    :: dict()  % InPort -> {EndTime, Value}
  }).

start_link(Name, Bindings, Module, ModArgs) ->
  gen_server:start_link({via, clb_afsm_registry, Name}, ?MODULE, {Name, Bindings, Module, ModArgs}, []).

init({Name, Bindings, Module, ModArgs}) ->
  case Module:init(Name, ModArgs) of
    {ok, AFSMState, InputPorts, OutputPorts, ModState} ->
      {ok, #state{
          name=Name,
          module=Module,
          mod_state=ModState,
          afsm_state=AFSMState,
          in_ports=InputPorts,
          out_ports=OutputPorts,
          dispatchtab=build_dispatch_table(Bindings, Name),
          inhibitortab=build_inhibitor_table(Bindings, Name),
          supressortab=build_supressor_table(Bindings, Name),
          inhibited=dict:new(),
          supressed=dict:new()}
      };
    {error, _} = E ->
      E
  end.

handle_call(_Request, _From, State) ->
  Reply = ok,
  {reply, Reply, State}.

handle_cast({signal, normal, Value, Port, _From}, State=#state{name=Name, module=Module, afsm_state=AFSMState, mod_state=ModState, in_ports=InPorts}) ->
  case lists:member(Port, InPorts) of
    true ->
      case is_suppressed(Port, State) of
        false ->
          handle_callback_res(Module:handle_signal(Value, Port, AFSMState, ModState), State);
        {true, SValue} ->
          handle_callback_res(Module:handle_signal(SValue, Port, AFSMState, ModState), State)
      end;
    false ->
      error_logger:error_report([{message, "Invalid input port."}, {receiver_name, Name}, {receiver_port, Port}, {value, Value}]),
      {noreply, State}
  end;
handle_cast({signal, inhibits, Value, Port, From}, State=#state{name=Name, out_ports=OutPorts, inhibitortab=InhibitorTab}) ->
  case lists:member(Port, OutPorts) of
    true ->
      case dict:find(Port, InhibitorTab) of
        {ok, Times} ->
          case lists:keyfind(From, 1, Times) of
            {_From, T} ->
              NewState = inhibit_port(Port, T, State),
              {noreply, NewState}
          end
      end;
    false ->
      error_logger:error_report([{message, "Invalid output port."}, {receiver_name, Name}, {receiver_port, Port}, {value, Value}]),
      {noreply, State}
  end;
handle_cast({signal, suppresses, Value, Port, From}, State=#state{name=Name, in_ports=InPorts, supressortab=SupressorTab}) ->
  case lists:member(Port, InPorts) of
    true ->
      case dict:find(Port, SupressorTab) of
        {ok, Times} ->
          case lists:keyfind(From, 1, Times) of
            {_From, T} ->
              NewState = supress_port(Port, T, Value, State),
              {noreply, NewState}
          end
      end;
    false ->
      error_logger:error_report([{message, "Invalid input port."}, {receiver_name, Name}, {receiver_port, Port}, {value, Value}]),
      {noreply, State}
  end.

handle_info(Info, State=#state{module=Module, afsm_state=AFSMState, mod_state=ModState}) ->
  handle_callback_res(Module:handle_info(Info, AFSMState, ModState), State).

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% Internal functions

handle_callback_res({ok, Outputs, AFSMState, ModState}, State) ->
  [send_signal(Port, Value, State) || {Port, Value} <- Outputs],
  {noreply, State#state{afsm_state=AFSMState, mod_state=ModState}};
handle_callback_res({ok, Outputs, ModState}, State=#state{afsm_state=AFSMState}) ->
  handle_callback_res({ok, Outputs, AFSMState, ModState}, State);
handle_callback_res({ok, ModState}, State) ->
  handle_callback_res({ok, [], ModState}, State);
handle_callback_res({stop, Reason, NewState}, State) ->
  {stop, Reason, State=#state{mod_state=NewState}}.

signal(ServerRef, SigType, Value, Port, From) ->
  %% io:format("signal(~p, ~p, ~p, ~p, ~p)~n", [ServerRef, SigType, Value, Port, From]),
  gen_server:cast(ServerRef, {signal, SigType, Value, Port, From}).

send_signal(Port, Value, State=#state{name=Name, out_ports=OutPorts, dispatchtab=DispatchTab}) ->
  case lists:member(Port, OutPorts) of
    true ->
      case is_inhibited(Port, State) of
        false ->
          case dict:find(Port, DispatchTab) of
            {ok, Dests} ->
              lists:foreach(fun({SigType, {DstName, DstPort}}) ->
                ServerRef = clb_afsm_registry:whereis_name(DstName),
                signal(ServerRef, SigType, Value, DstPort, {Name, Port})
              end, Dests);
            error ->
              ok
          end;
        true ->
          ok
      end;
    false ->
      error_logger:error_report([{message, "Invalid output port."}, {sender_name, Name}, {sender_port, Port}, {value, Value}])
  end.

build_dispatch_table(Bindings, Name) ->
  lists:foldl(
    fun
      ({normal, {SrcName, SrcPort}, Dst}, Acc) when SrcName == Name ->
        dict:append(SrcPort, {normal, Dst}, Acc);
      ({inhibiting, _T, {InhibitorName, InhibitorPort}, Dst}, Acc) when InhibitorName == Name ->
        dict:append(InhibitorPort, {inhibits, Dst}, Acc);
      ({supressing, _T, {SupressorName, SupressorPort}, Dst}, Acc) when SupressorName == Name ->
        dict:append(SupressorPort, {suppresses, Dst}, Acc);
      (_, Acc) ->
        Acc
    end, dict:new(), Bindings).

build_inhibitor_table(Bindings, Name) ->
  lists:foldl(
    fun
      ({inhibiting, T, Inhibitor, {InhibitedName, InhibitedPort}}, Acc) when InhibitedName == Name ->
        dict:append(InhibitedPort, {Inhibitor, T}, Acc);
      (_, Acc) ->
        Acc
    end, dict:new(), Bindings).

build_supressor_table(Bindings, Name) ->
  lists:foldl(
    fun
      ({supressing, T, Supressor, {TargetName, TargetPort}}, Acc) when TargetName == Name ->
        dict:append(TargetPort, {Supressor, T}, Acc);
      (_, Acc) ->
        Acc
    end, dict:new(), Bindings).

inhibit_port(Port, T, State=#state{name=Name, inhibited=Inhibited}) ->
  io:format("inhibiting port ~p for ~p secs (~p)~n", [Port, T, Name]),
  NewInh = dict:store(Port, cur_timestamp()+T, Inhibited),
  State#state{inhibited=NewInh}.

supress_port(Port, T, Value, State=#state{name=Name, supressed=Supressed}) ->
  io:format("suppressing port ~p with value ~p for ~p secs (~p)~n", [Port, Value, T, Name]),
  NewSup = dict:store(Port, {cur_timestamp()+T, Value}, Supressed),
  State#state{supressed=NewSup}.

is_inhibited(Port, #state{inhibited=Inhibited}) ->
  case dict:find(Port, Inhibited) of
    {ok, EndTime} ->
      cur_timestamp() < EndTime;
    error ->
      false
  end.

is_suppressed(Port, #state{supressed=Supressed}) ->
  case dict:find(Port, Supressed) of
    {ok, {EndTime, Val}} ->
      case cur_timestamp() < EndTime of
        false ->
          false;
        true ->
          {true, Val}
      end;
    error ->
      false
  end.

cur_timestamp() ->
  {MegaSecs, Secs, MicroSecs} = os:timestamp(),
  MegaSecs*1000000 + Secs + MicroSecs/1000000.

% TODO: tests
