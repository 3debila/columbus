-ifndef(_CLB_CONTROLLER_PROTO_).
-define(_CLB_CONTROLLER_PROTO_, true).

%% FrameCodes:
%% ==========
%%
%% Cmd frames:
%% 1 = get
%% 2 = set 
%% 3 = collect_aggregated
%% 4 = trigger_threshold
%% 5 = cancel_trigger
%%
%% Reserved frames:
%% 0 = resp_frame
%% 126 = debug_frame
%% 127 = trigger_frame

-define(VSN, 0).
-define(UID_SZ, 16).
-define(FRAME_CODE_SZ, 7).
-define(PAYLOADSZ_SZ, 8).

-define(HSHK_FRAME, 0).
-define(NTRI_FRAME, 124).
-define(RESP_FRAME, 125).
-define(DEBG_FRAME, 126).
-define(TRIG_FRAME, 127).

-define(INT, little-integer).

%% Device must send `?HANDSHAKE' after a
%% communication was esablished.
-define(HANDSHAKE(Rest),
        <<?VSN:1/?INT,
          ?HSHK_FRAME:?FRAME_CODE_SZ/?INT,
          (Rest)/binary>>).

%% Controller sends `?CMD_FRAME' to a device as a request
-define(CMD_FRAME(OpCode, UniqId, PayloadSize, Payload),
        <<?VSN:1/?INT,
          (OpCode):?FRAME_CODE_SZ/?INT,
          (UniqId):?UID_SZ/?INT,
          (PayloadSize):?PAYLOADSZ_SZ/?INT,
          (Payload)/binary>>).

%% If during the execution of the request
%% one or more triggers were created, device must 
%% send a `?NEW_TRIGGER_FRAME' for each of them.
%% Where `UniqId' is the `UniqId' of the 
%% corresponding `?CMD_FRAME' and `TriggerId' is
%% a unique identifier of a created trigger.
%% (`TriggerId' may be reused if a previous trigger
%% with that identifier was cancelled.)
-define(NEW_TRIGGER_FRAME(UniqId, TriggerId, Rest),
        <<?VSN:1/?INT,
          ?NTRI_FRAME:?FRAME_CODE_SZ/?INT,
          (UniqId):?UID_SZ/?INT,
          (TriggerId):?UID_SZ/?INT,
          (Rest)/binary>>).

%% Then the device must respond with a `?RESP_FRAME'
%% that may contain arbitrary data.
%% (Currently response with no data at all is
%% used to indicate that request is understood
%% but something is wrong. In such case additional
%% details are sent via `?DEBUG_FRAME').
%% Where `UniqId' is the `UniqId' of the 
%% corresponding `?CMD_FRAME'.
-define(RESP_FRAME(UniqId, PayloadSize, Payload),
        <<?VSN:1/?INT,
          ?RESP_FRAME:?FRAME_CODE_SZ/?INT,
          (UniqId):?UID_SZ/?INT,
          (PayloadSize):?PAYLOADSZ_SZ/?INT,
          (Payload)/binary>>).

%% `?TRIGGER_FRAME' is sent by a device when 
%% a corresponding trigger is fired.
-define(TRIGGER_FRAME(TriggerId, Rest),
        <<?VSN:1/?INT,
          ?TRIG_FRAME:?FRAME_CODE_SZ/?INT,
          (TriggerId):?UID_SZ/?INT,
          (Rest)/binary>>).

%% `?DEBUG_FRAME' with an arbitrary data may be
%% sent by device at any time and is expected 
%% to be written to a log file.
-define(DEBUG_FRAME(PayloadSize, Payload),
        <<?VSN:1/?INT,
          ?DEBG_FRAME:?FRAME_CODE_SZ/?INT,
          (PayloadSize):?PAYLOADSZ_SZ/?INT,
          (Payload)/binary>>).

%% TODO ping/pong

-endif.
