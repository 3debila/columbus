-module(clb_afsm_hwtrigger).
-behaviour(clb_afsm).
-export([init/2, handle_signal/4, handle_info/3]).
-include("../include/clb_controller.hrl").

-record(state, {controller_mon}).

init(_Name, Props) -> 
    [Device, Channel, Sign, Value] = 
        [proplists:get_value(K, Props) || K <- [device, channel, sign, value]],
    Controller = clb_device_info:get_param(Device, controller),
    Controller:trigger_threshold(Device, Channel, Sign, Value),
    MonitorRef = Controller:monitor(Device),
    {ok, the_only_state, [], [out], #state{controller_mon=MonitorRef}}.

handle_signal(_Val, in, the_only_state, ModuleState) ->
    {ok, ModuleState}.

handle_info(?TRIGGER(_), the_only_state, State) ->
    {ok, [{out, 1}], State};
handle_info({'DOWN', MonitorRef, _, _, _}, the_only_state, #state{controller_mon=MonitorRef}) ->
    exit(controller_down);
handle_info(_, the_only_state, ModuleState) ->
    {ok, ModuleState}.

