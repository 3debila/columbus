#include <ooPinChangeInt.h>
#include <AdaEncoder.h>
#include "ColumbusProto.h"
#define MAX_ANALOG 1024.0f

#define ANALOG_IR_DIST_PIN 0
#define CHAN_IR_DIST 1

#define DIGITAL_IR_OBST_PIN 2
#define CHAN_IR_OBST 2

#define WHEEL_1_PIN 4
#define WHEEL_2_PIN 5
#define WHEEL_3_PIN 6
#define WHEEL_4_PIN 7

#define CHAN_WHEEL_CONTROL 3

#define ENCODER_LEFT_1_PIN 10
#define ENCODER_LEFT_2_PIN 11
#define ENCODER_RIGHT_1_PIN 9
#define ENCODER_RIGHT_2_PIN 8

#define CHAN_ENCODER_LEFT 5
#define CHAN_ENCODER_RIGHT 6

#define LED_PIN 13
#define CHAN_LED 4

#define MAX_CHANNELS 32
#define MAX_TRIGGERS 32

AdaEncoder encoderLeft = AdaEncoder('l', ENCODER_LEFT_1_PIN, ENCODER_LEFT_2_PIN);
AdaEncoder encoderRight = AdaEncoder('r', ENCODER_RIGHT_1_PIN, ENCODER_RIGHT_2_PIN);

template <class SendT>
class GettersHandler {
	SendT &send;
	int readable_chans[MAX_CHANNELS];

public:
	GettersHandler(SendT &s) : send(s) {
		memset(readable_chans, 0, sizeof(readable_chans));
		pinMode(ANALOG_IR_DIST_PIN, INPUT);
		pinMode(DIGITAL_IR_OBST_PIN, INPUT);
	}

	void update() {
		readable_chans[CHAN_IR_DIST] = analogRead(ANALOG_IR_DIST_PIN);
		readable_chans[CHAN_IR_OBST] = digitalRead(DIGITAL_IR_OBST_PIN)*1024;

		AdaEncoder *thisEncoder=NULL;
		thisEncoder=AdaEncoder::genie();
		if (thisEncoder != NULL) {
			if (thisEncoder->getID() == 'l')
			{
				readable_chans[CHAN_ENCODER_LEFT] += thisEncoder->query();
			}
			else if (thisEncoder->getID() == 'r')
			{
				readable_chans[CHAN_ENCODER_RIGHT] += thisEncoder->query();
			}
		}
	}

	void handle_get(unsigned chan, uint16_t uniq_id) {
		if (chan > MAX_CHANNELS) {
			send.report_error(uniq_id, "get: no such channel");
			return;
		}
		send.result(uniq_id, readable_chans[chan]);
	}

	void handle_collect(unsigned chan, uint16_t uniq_id) {
		if (chan > MAX_CHANNELS) {
			send.report_error(uniq_id, "collect: no such channel");
			return;
		}
		send.result(uniq_id, readable_chans[chan]);
		readable_chans[chan] = 0;
	}

	int value(int chan) {
		return readable_chans[chan];
	}
};

template <class SendT>
class SettersHandler {
	SendT &send;

	typedef bool (*chan_setter)(int16_t);
	chan_setter setters[MAX_CHANNELS];


	static bool wheel_control_set(int16_t val) {
		static const int dirPatterns[][4] = {
			{LOW, LOW, LOW, LOW},	// 0: stall
			{HIGH, LOW, HIGH, LOW}, // 1: fwd
			{LOW, HIGH, LOW, HIGH}, // 2: bwd
			{HIGH, LOW, LOW, HIGH}, // 3: left
			{LOW, HIGH, HIGH, LOW}  // 4: right
		};

		if (val < 0 || val > 4) {
			return false;
		}

		digitalWrite(WHEEL_1_PIN, dirPatterns[val][0]);
		digitalWrite(WHEEL_2_PIN, dirPatterns[val][1]);
		digitalWrite(WHEEL_3_PIN, dirPatterns[val][2]);
		digitalWrite(WHEEL_4_PIN, dirPatterns[val][3]);

		return true;
	}

	static bool led_set(int16_t val) {
		digitalWrite(LED_PIN, (val > 0) ? HIGH : LOW);
		return true;
	}

public:
	SettersHandler(SendT &s) : send(s) {
		memset(setters, 0, sizeof(setters));
		setters[CHAN_WHEEL_CONTROL] = wheel_control_set;
		setters[CHAN_LED] = led_set;
		pinMode(WHEEL_1_PIN, OUTPUT);
		pinMode(WHEEL_2_PIN, OUTPUT);
		pinMode(WHEEL_3_PIN, OUTPUT);
		pinMode(WHEEL_4_PIN, OUTPUT);
		pinMode(LED_PIN, OUTPUT);
	}

	bool set(unsigned chan, int16_t value)
	{
		//TODO: DRY
		if (chan > MAX_CHANNELS) {
			return false;
		}
		chan_setter setter = setters[chan];
		if (setter) {
			return setter(value);
		}
		else {
			return false;
		}
	}

	void handle_set(unsigned chan, uint16_t uniq_id, int16_t value) {
		if (chan > MAX_CHANNELS) {
			send.report_error(uniq_id, "set: no such channel");
			return;
		}
		chan_setter setter = setters[chan];
		if (setter) {
			bool val = setter(value);
			if (val) send.result(uniq_id, val);
			else send.report_error(uniq_id, "set: set failed");
		}
		else {
			send.report_error(uniq_id, "set: no setter is defined for this channel");
		}
	}
};

template <class SendT, class GettersT, class SettersT>
class TriggersHandler {
	SendT &send;
	GettersT &getters;
	SettersT &setters;

	struct TriggerContext {
		int out_chan;
		int out_thres;
		int in_chan;
		int in_val;
		int sign;
		int reacted;

		TriggerContext() {
			out_chan = -1;
			out_thres = -1;
			in_chan = -1;
			in_val = -1;
			sign = 0;
			reacted = 0;
		}
	};

	TriggerContext triggers[MAX_TRIGGERS];
public:
	TriggersHandler(SendT &s, GettersT &gt, SettersT &st) : send(s), getters(gt), setters(st) {}

	void update()
	{
		for (int i=0; i<MAX_TRIGGERS; ++i) {
			int chan = triggers[i].out_chan;

			if (chan == -1)
				continue;

			bool threshold = ((triggers[i].sign > 0 && getters.value(chan) >= triggers[i].out_thres) ||
				(triggers[i].sign < 0 && getters.value(chan) <= triggers[i].out_thres));
			if (threshold && !triggers[i].reacted) {
				triggers[i].reacted = 1;
				if (triggers[i].in_chan != -1)
					setters.set(triggers[i].in_chan, triggers[i].in_val);
				send.trigger(uint16_t(i));
			}
			else if (!threshold && triggers[i].reacted) {
				triggers[i].reacted = 0;
			}
		}
	}

	void handle_trigger_threshold(uint16_t uniq_id, unsigned out_chan, int sign, int out_thres, unsigned in_chan, int in_val) {
		for (int i=0; i<MAX_TRIGGERS; ++i) {
			if (triggers[i].out_chan != -1)
				continue;

			triggers[i].out_chan = out_chan;
			triggers[i].out_thres = out_thres;
			triggers[i].in_chan = in_chan;
			triggers[i].in_val = in_val;
			triggers[i].sign = sign;
			triggers[i].reacted = 0;

			send.new_trigger(uniq_id, i);
			send.result(uniq_id, uint16_t(i));
			return;
		}
		send.report_error(uniq_id, "handle_trigger_threshold: no available trigger slots");
	}

	void handle_cancel_trigger(uint16_t uniq_id, uint16_t triggerid) {
		if (triggerid >= MAX_TRIGGERS) {
			send.report_error(uniq_id, "handle_cancel_trigger: invalid triggerid");
			return;
		}

		if (triggers[triggerid].out_chan == -1) {
			send.report_error(uniq_id, "handle_cancel_trigger: trigger already disabled");
			return;
		}

		triggers[triggerid].out_chan = -1;
		send.result(uniq_id, uint8_t(1));
	}
};

class Handler {
	// communication

	typedef typeof(Serial) serial_t;
	typedef columbus::send<serial_t> send_t;
	send_t send;

	GettersHandler<send_t> getters;
	SettersHandler<send_t> setters;
	TriggersHandler<send_t, GettersHandler<send_t>, SettersHandler<send_t> > triggers;

public:
	Handler() : send(Serial), getters(send), setters(send), triggers(send, getters, setters) {}

	void handshake() {
		send.handshake();
	}

	void update() {
		getters.update();
		triggers.update();
	}

	void on_message(columbus::opcode op, uint16_t uniq_id, uint8_t size, char *bytes) {
		switch (op) {
			case columbus::get:
				{
					if (size != 1) {
						send.report_error(uniq_id, "get: wrong size of an argument!");
						return;
					}
					getters.handle_get(bytes[0], uniq_id);
					break;
				}
			case columbus::set:
				{
					if (size != 3) {
						send.report_error(uniq_id, "set: wrong size of an argument!");
					   	return;
					}
					char chan = bytes[0];
					int16_t value = *reinterpret_cast<int16_t *>(bytes+1);
					setters.handle_set(chan, uniq_id, value);
					break;
				}
			case columbus::collect_value:
				{
					if (size != 1) {
						send.report_error(uniq_id, "get: wrong size of an argument!");
						return;
					}
					getters.handle_collect(bytes[0], uniq_id);
					break;
				}
			case columbus::trigger_threshold:
				{
					if (size != 7) {
						send.report_error(uniq_id, "trigger_threshold: wrong size of an argument!");
						return;
					}
					char out_c = bytes[0];
					char sign = bytes[1];
					int16_t out_thres = *reinterpret_cast<int16_t *>(bytes+2);
					char in_c = bytes[4];
					int16_t in_val = *reinterpret_cast<int16_t *>(bytes+5);
					triggers.handle_trigger_threshold(uniq_id, out_c, sign, out_thres, in_c, in_val);
					break;
				}
			case columbus::cancel_trigger:
				{
					if (size != 2) {
						send.report_error(uniq_id, "cancel_trigger: wrong size of an argument!");
						return;
					}
					uint16_t trig_id = *reinterpret_cast<uint16_t *>(bytes);
					triggers.handle_cancel_trigger(uniq_id, trig_id);
					break;
				}
			default:
				send.debug("uknown operation");
				send.debug(op);
				send.result_null(uniq_id);
				break;
		}
	}
};

columbus::receive<Handler> p;

void setup() {
	Serial.begin(115200);
	p.handler().handshake();
}

void loop() {
	p.handler().update();
}

void serialEvent() {
	p.process_bytes(Serial);
}
