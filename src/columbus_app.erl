-module(columbus_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-export([start/0]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->
    ok = application:start(compiler, permanent),
    ok = application:start(syntax_tools, permanent),
    ok = application:start(goldrush, permanent),
    ok = application:start(lager, permanent),
    ok = application:start(columbus, transient).

start(_StartType, _StartArgs) ->
    columbus_sup:start_link().

stop(_State) ->
    ok.
