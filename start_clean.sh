#!/bin/sh

TTY=$1
[ $TTY ] || TTY="/dev/tty.usbmodem1411"
erl -pa ebin -pa deps/*/ebin -boot start_sasl -config app.config -s sync -eval 'application:load(columbus).' -s lager -columbus duino_serial_tty \"$TTY\"
