#!/bin/sh

TTY=$1
[ $TTY ] || TTY="/dev/tty.usbmodem1411"
CONF='[{arduino, [{tty, "'$TTY'"}, {controller, clb_controller_duino}]}]'
erl -pa ebin -pa deps/*/ebin -boot start_sasl -config app.config -s sync -columbus devices "$CONF" -s columbus_app 
