-module(clb_afsm_debug).

-behaviour(clb_afsm).

-export([init/2, handle_signal/4, handle_info/3]).
-export([generate_signal/2]).

init(Name, _Args) ->
  {ok, nullstate, [in], [out], [{name, Name}]}.

handle_signal(Val, in, nullstate, State) ->
  io:format("Got signal ~p (~p)~n", [Val, State]),
  {ok, State}.

handle_info({sig, Val}, nullstate, State) ->
  {ok, [{out, Val}], nullstate, State}.

generate_signal(Name, Val) ->
  Ref = clb_afsm_registry:whereis_name(Name),
  Ref ! {sig, Val},
  ok.
