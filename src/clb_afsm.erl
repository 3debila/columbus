-module(clb_afsm).
-export_type([binding/0, system/0]).

-type value() :: term().
-type time_const() :: non_neg_integer().

-type afsm_name() :: term().
-type afsm_port() :: term().

%% `Mod' exposes its ports thru `init'
-type afsm_spec() :: {proc, Mod::module(), proplists:proplist()}. 

-type afsm_inst() :: {afsm_name(), afsm_spec()}.
-type afsm_endpoint() :: {afsm_name(), afsm_port()}.

-type binding() :: {normal,
                    Source    :: afsm_endpoint(),
                    Target    :: afsm_endpoint()}
                 | {supressing, time_const(), 
                    Supressor :: afsm_endpoint(),
                    Target    :: afsm_endpoint()}
                 | {inhibiting, time_const(),
                    Inhibitor :: afsm_endpoint(),
                    Inhibited :: afsm_endpoint()}.

-type system() :: {system, [afsm_inst()], [binding()]}.

-type mod_state() :: term(). 
-type afsm_state() :: atom().
-type afsm_outputs() :: [{afsm_port(), value()}].
-type afsm_callback_res() :: {ok, afsm_outputs(), afsm_state(), mod_state()} 
                           | {ok, afsm_outputs(), mod_state()} 
                           | {ok, mod_state()}
                           | {stop, Reason::term(), mod_state()}.
-type afsm_input_ports() :: [afsm_port()].
-type afsm_output_ports() :: [afsm_port()].

%% A module that implements the base behaviour for AFSMs
%% receives a filtered list of bindings.
%% That list must contain:
%%  - all `normal' bindings where the given worker is a `Source';
%%  - all `supressing' and `inhibiting' bindings where 
%%  the given worker is a `Target'/`Inhibited'.

-callback init(afsm_name(), proplists:proplist()) ->
    {ok,
     afsm_state(),
     afsm_input_ports(),
     afsm_output_ports(),
     mod_state()} 
  | {error, term()}.
-callback handle_signal(value(), afsm_port(), afsm_state(), mod_state()) ->
    afsm_callback_res().
-callback handle_info(term(), afsm_state(), mod_state()) ->
    afsm_callback_res().


